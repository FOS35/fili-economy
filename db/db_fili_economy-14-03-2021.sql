-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15-Mar-2021 às 00:29
-- Versão do servidor: 10.1.40-MariaDB
-- versão do PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fili_economy`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alterar_usuario` (IN `vid_usuario` INT, IN `pemail` VARCHAR(255), IN `psenha` VARCHAR(255))  NO SQL
BEGIN

UPDATE usuario SET email = pemail, senha = psenha WHERE id_usuario = vid_usuario;

SELECT * FROM usuario WHERE id_usuario = vid_usuario;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_salvar_novomes` (IN `pmes_orc` CHAR(2), IN `palimentacao` DECIMAL(5,2), IN `pprimeira_parc` DECIMAL(5,2), IN `pbonus` DECIMAL(5,2), IN `psegunda_parc` DECIMAL(5,2), IN `pano_orc` CHAR(4), IN `vid_usuario` INT)  NO SQL
BEGIN

DECLARE vidmes INT;

INSERT INTO mes (mes_orc, ano_orc, id_usuario) VALUES (pmes_orc, pano_orc, vid_usuario);

SET vidmes = LAST_INSERT_ID();

INSERT INTO capital (alimentacao, primeira_parc, bonus, segunda_parc, id_mes_orc) VALUES (palimentacao, pprimeira_parc, pbonus, psegunda_parc, vidmes);

SELECT * FROM capital WHERE id_mes_orc = vidmes;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_salvar_produt_servico` (IN `pproduto` VARCHAR(255), IN `pvalor` DECIMAL(5,2), IN `pdata_compra` DATE, IN `vid_mes` INT)  NO SQL
BEGIN

INSERT INTO produto (produto, valor, data_compra, id_mes_orc) VALUES (pproduto, pvalor, pdata_compra, vid_mes);

SELECT * FROM produto WHERE id_produto = LAST_INSERT_ID();

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_salvar_total` (IN `ptotal` DECIMAL(8,2), IN `vid_mes_orc` INT)  NO SQL
BEGIN

UPDATE capital SET total = ptotal;

SELECT * FROM capital WHERE id_mes_orc = vid_mes_orc;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_salvar_usuario` (IN `pemail` VARCHAR(255), IN `pusuario` VARCHAR(255), IN `psenha` VARCHAR(255), IN `psts` CHAR(1), IN `padmin` CHAR(1))  NO SQL
BEGIN

INSERT INTO usuario (email, usuario, senha, sts, admin) VALUES (pemail, pusuario, psenha, psts, padmin);
    
    SELECT * FROM usuario WHERE id_usuario = LAST_INSERT_ID();

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `up_editar_novo_mes` (IN `palimentacao` DECIMAL(5,2), IN `pprimeira_parc` DECIMAL(5,2), IN `pbonus` DECIMAL(5,2), IN `psegunda_parc` DECIMAL(5,2), IN `vid_capital` INT)  NO SQL
BEGIN

UPDATE capital SET alimentacao = palimentacao, primeira_parc = pprimeira_parc, bonus = pbonus, segunda_parc = psegunda_parc WHERE id_capital = vid_capital;

SELECT * FROM capital WHERE id_capital = vid_capital;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `capital`
--

CREATE TABLE `capital` (
  `id_capital` int(11) NOT NULL,
  `alimentacao` decimal(5,2) DEFAULT NULL,
  `primeira_parc` decimal(5,2) DEFAULT NULL,
  `bonus` decimal(5,2) DEFAULT NULL,
  `segunda_parc` decimal(5,2) DEFAULT NULL,
  `total` decimal(8,2) NOT NULL,
  `id_mes_orc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `capital`
--

INSERT INTO `capital` (`id_capital`, `alimentacao`, `primeira_parc`, `bonus`, `segunda_parc`, `total`, `id_mes_orc`) VALUES
(6, '400.00', '900.00', '400.00', '700.00', '602.00', 6),
(7, '450.00', '1.20', '480.00', '730.00', '602.00', 7),
(8, '20.00', '20.00', '20.00', '20.00', '602.00', 8),
(9, '200.00', '2.00', '200.00', '200.00', '602.00', 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mes`
--

CREATE TABLE `mes` (
  `id_mes` int(11) NOT NULL,
  `mes_orc` char(2) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ano_orc` char(4) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mes`
--

INSERT INTO `mes` (`id_mes`, `mes_orc`, `data_criacao`, `ano_orc`, `id_usuario`) VALUES
(6, '02', '2021-02-10 22:48:49', '2021', 9),
(7, '03', '2021-02-10 23:47:35', '2021', 9),
(8, '01', '2021-02-26 21:25:56', '2022', 9),
(9, '01', '2021-02-26 21:30:55', '2021', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id_produto` int(11) NOT NULL,
  `produto` varchar(255) NOT NULL,
  `valor` decimal(5,2) NOT NULL,
  `data_compra` date NOT NULL,
  `id_mes_orc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id_produto`, `produto`, `valor`, `data_compra`, `id_mes_orc`) VALUES
(1, 'Conta de Luz', '60.00', '2021-02-05', 6),
(2, 'Conta de Ã¡gua', '28.00', '2021-02-04', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `admin` char(1) NOT NULL,
  `sts` char(1) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `email`, `senha`, `admin`, `sts`, `data_cadastro`) VALUES
(9, 'filipe', 'filipeodev@gmail.com', '$2y$12$qaVaPfAx/yj0TBd2vmUodeeLAMDSwh.W73kQQM6kWalXXpqBw05we', '0', '1', '2021-02-02 01:49:13'),
(10, 'teste', 'teste@gmail.com', '$2y$12$oZae.oQ3mt3dHtxl2yJiVu2tF6ZZe8hGNlBqcxsMkADQFULWU53Ru', '0', '1', '2021-02-05 00:39:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `capital`
--
ALTER TABLE `capital`
  ADD PRIMARY KEY (`id_capital`),
  ADD KEY `fk_tb_capital_mes` (`id_mes_orc`) USING BTREE;

--
-- Indexes for table `mes`
--
ALTER TABLE `mes`
  ADD PRIMARY KEY (`id_mes`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id_produto`),
  ADD KEY `fk_tb_produto_mes` (`id_mes_orc`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `capital`
--
ALTER TABLE `capital`
  MODIFY `id_capital` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mes`
--
ALTER TABLE `mes`
  MODIFY `id_mes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `capital`
--
ALTER TABLE `capital`
  ADD CONSTRAINT `fk_tb_capital_mes` FOREIGN KEY (`id_mes_orc`) REFERENCES `mes` (`id_mes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `fk_tb_produto_mes` FOREIGN KEY (`id_mes_orc`) REFERENCES `mes` (`id_mes`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
